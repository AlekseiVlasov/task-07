package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static final String SQL_INSERT_USER = "INSERT INTO users (login) VALUES(?)";
	private static final String SQL_INSERT_TEAM = "INSERT INTO teams (name) VALUES(?)";
	private static final String SQL_SELECT_ALL_USERS = "SELECT * FROM users";
	private static final String SQL_SELECT_ALL_TEAMS = "SELECT * FROM teams";
	private static final String SQL_GET_USER = "SELECT id,login FROM users WHERE login=?";
	private static final String SQL_GET_TEAM = "SELECT id,name FROM teams WHERE name=?";
	private static final String SQL_DELETE_USER = "DELETE FROM users WHERE login=?";
	private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name=?";
	private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";
	private static final String SQL_SET_TEAMS_FOR_USER = "INSERT INTO users_teams(user_id,team_id) SELECT users.id,teams.id FROM users,teams WHERE users.login=? AND teams.name=?";
	private static final String SQL_GET_USER_TEAMS = "SELECT name,id FROM teams JOIN users_teams AS ut ON teams.id = ut.team_id AND user_id IN (SELECT id FROM users WHERE login=?)";
	private static DBManager instance;
	private String dbUrl;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {

	}

	public List<User> findAllUsers() throws DBException {

		List<User> list = new ArrayList<User>();
		try (Connection conect = getConnection(); Statement st = conect.createStatement()) {

			st.executeQuery(SQL_SELECT_ALL_USERS);

			ResultSet generatedKeys = st.getResultSet();

			while (generatedKeys.next()) {
				User temp = User.createUser(generatedKeys.getString(2));
				temp.setId(generatedKeys.getInt(1));
				list.add(temp);
			}
			return list;
		} catch (SQLException e) {
			throw new DBException("Fail while finding all users", e);
		}
	}

	public boolean insertUser(User user) throws DBException {

		try (Connection conect = getConnection();
				PreparedStatement st = conect.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
			st.setString(1, user.getLogin());

			int rows = st.executeUpdate();

			ResultSet generatedKeys = st.getGeneratedKeys();

			if (generatedKeys.next()) {
				user.setId(generatedKeys.getInt(1));
			}
			return rows > 0;
		} catch (SQLException e) {
			throw new DBException("Fail during insert in users", e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {

		try (Connection conect = getConnection(); PreparedStatement st = conect.prepareStatement(SQL_DELETE_USER)) {
			conect.setAutoCommit(false);
			for (User user : users) {
				st.setString(1, user.getLogin());
				if (st.executeUpdate() != 1) {
					conect.rollback();
					return false;
				}
			}
			conect.commit();
			return true;
		} catch (SQLException e) {

			throw new DBException("Transaction on delete userfail", e);
		}

	}

	public User getUser(String login) throws DBException {

		try (Connection conect = getConnection();
				PreparedStatement st = conect.prepareStatement(SQL_GET_USER, Statement.RETURN_GENERATED_KEYS)) {
			st.setString(1, login);
			st.execute();
			ResultSet generatedKeys = st.getResultSet();

			if (generatedKeys.next()) {
				User temp = User.createUser(generatedKeys.getString(2));
				temp.setId(generatedKeys.getInt(1));
				return temp;
			}
		} catch (SQLException e) {
			throw new DBException("Fail to get user from users", e);
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		try (Connection conect = getConnection();
				PreparedStatement st = conect.prepareStatement(SQL_GET_TEAM, Statement.RETURN_GENERATED_KEYS)) {
			st.setString(1, name);
			st.execute();
			ResultSet generatedKeys = st.getResultSet();

			if (generatedKeys.next()) {
				Team temp = Team.createTeam(generatedKeys.getString(2));
				temp.setId(generatedKeys.getInt(1));
				return temp;
			}
		} catch (SQLException e) {
			throw new DBException("Fail to get user from users", e);
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> list = new ArrayList<Team>();
		try (Connection conect = getConnection(); Statement st = conect.createStatement()) {

			st.executeQuery(SQL_SELECT_ALL_TEAMS);

			ResultSet generatedKeys = st.getResultSet();

			while (generatedKeys.next()) {
				Team temp = Team.createTeam(generatedKeys.getString(2));
				temp.setId(generatedKeys.getInt(1));
				list.add(temp);
			}
			return list;
		} catch (SQLException e) {
			throw new DBException("Fail while finding all users", e);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection conect = getConnection();
				PreparedStatement st = conect.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
			st.setString(1, team.getName());

			int rows = st.executeUpdate();

			ResultSet generatedKeys = st.getGeneratedKeys();

			if (generatedKeys.next()) {
				team.setId(generatedKeys.getInt(1));
			}
			return rows > 0;
		} catch (SQLException e) {
			throw new DBException("Fail during insert in teams", e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		if (user == null || teams == null)
			return false;
		try (Connection conect = getConnection();
				PreparedStatement st = conect.prepareStatement(SQL_SET_TEAMS_FOR_USER)) {
			conect.setAutoCommit(false);
			try {
				for (Team team : teams) {
					st.setString(1, user.getLogin());
					st.setString(2, team.getName());
					st.executeUpdate();

				}
				conect.commit();
			} catch (Exception e) {
				conect.rollback();
				throw new DBException("Transaction fail", e);
			} finally {
				st.close();
				conect.close();
			}

		} catch (SQLException e) {
			throw new DBException("Transaction fail", e);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> list = new ArrayList<Team>();
		try (Connection conect = getConnection(); PreparedStatement st = conect.prepareStatement(SQL_GET_USER_TEAMS)) {

			st.setString(1, user.getLogin());
			st.execute();

			ResultSet generatedKeys = st.getResultSet();

			while (generatedKeys.next()) {
				Team temp = Team.createTeam(generatedKeys.getString(1));
				temp.setId(generatedKeys.getInt(2));
				list.add(temp);
			}
			return list;
		} catch (SQLException e) {
			throw new DBException("Fail while finding all users", e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection conect = getConnection(); PreparedStatement st = conect.prepareStatement(SQL_DELETE_TEAM)) {

			st.setString(1, team.getName());
			return st.executeUpdate() == 1;

		} catch (SQLException e) {

			throw new DBException("Delete team fail", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try (Connection conect = getConnection(); PreparedStatement st = conect.prepareStatement(SQL_UPDATE_TEAM)) {

			st.setString(1, team.getName());
			st.setInt(2, team.getId());
			return st.executeUpdate() == 1;

		} catch (SQLException e) {

			throw new DBException("Update team fail", e);
		}
	}

	private String getDBUrl() throws DBException {

		if (dbUrl != null)
			return dbUrl;
		Properties pr = new Properties();
		try (FileInputStream input = new FileInputStream("app.properties")) {
			pr.load(input);
			dbUrl = pr.getProperty("connection.url");

		} catch (FileNotFoundException e) {
			throw new DBException("Can't find app.properties ", e);

		} catch (IOException e) {
			throw new DBException("IOExeption", e);
		}
		return dbUrl;
	}

	private Connection getConnection() throws DBException {
		try {
			Connection con = DriverManager.getConnection(getDBUrl());
			return con;
		} catch (SQLException | DBException e) {
			throw new DBException("Fail to get connection", e);
		}
	}
}
